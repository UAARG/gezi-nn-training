import os
#Disables GPU. Uncomment this to run on CPU if you don't have NVIDIA GPU
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import tensorflow as tf
import numpy as np
import keras
from keras.models import load_model
from keras.preprocessing.image import load_img, img_to_array, array_to_img
import math
from math import floor

#compatibility for tensorflow 2.2 GPU. Comment out if you don't have NVIDIA GPU.
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session =tf.compat.v1.InteractiveSession(config=config)

#loads model and declare empty list for images 
model = load_model("Gezi_Image_Recognition_v1_2")
list_of_images = []

#image indices are in numerical order (format: 0.png, 1.png, 2.png, 3.png....)
start_image = 950001
end_image = 950011

#loops from starting index to ending index, inferences NN and draws squares where markers are.
for i in range(start_image, end_image):
	#converts images to numpy array and normalizes them.
	image_path = "Images/testing/" + str(i) + ".png"
	image_in = load_img(image_path)
	image_array = img_to_array(image_in)
	x = [image_array]
	x = np.array(x)
	x /= 255
	
	#uses model to predict output
	y = model.predict(x)
	print(y)


	image_out = image_array
	#draw squares where markers are
	for i in range(len(image_array)):
		for j in range(len(image_array)):
			if i > floor((y[0][0]-y[0][2]/2) * 480) and i < floor((y[0][0]+y[0][2]/2) * 480) and  j > floor((y[0][1]-y[0][3]/2) * 480)  and j < floor((y[0][1]+y[0][3]/2) * 480):
				image_array[j][i][0] = 255


	#saves images to list
	image_out = array_to_img(image_out)
	list_of_images.append(image_out)

#displays all images.
for i in list_of_images:
	i.show()

