import os
#Disables GPU. Uncomment this to run on CPU if you don't have NVIDIA GPU
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


import tensorflow as tf
import numpy as np
import keras 
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation, Conv2D, MaxPooling2D, BatchNormalization
from keras.preprocessing.image import load_img, img_to_array
from keras.optimizers import Adam, SGD
from keras.regularizers import l2
from keras.backend.tensorflow_backend import set_session, clear_session, get_session

#imports annotations.py
from annotations import *

#compatibility for tensorflow 2.2 GPU. Comment out if you don't have an NVIDIA GPU
config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session =tf.compat.v1.InteractiveSession(config=config)



#NN class
class ML:
	def __init__(self):
		self.model = Sequential()

		self.model.add(Conv2D(filters=32, kernel_size=(5, 5), strides=(2, 2), input_shape= (480, 480, 3), kernel_regularizer=l2(0.0001), bias_regularizer=l2(0.0001)))
		self.model.add(BatchNormalization())
		self.model.add(Activation('relu'))
		self.model.add(MaxPooling2D(pool_size=(2, 2)))

		self.model.add(Conv2D(filters=64, kernel_size=(5, 5), strides=(2, 2), kernel_regularizer=l2(0.0001), bias_regularizer=l2(0.0001)))
		self.model.add(BatchNormalization())
		self.model.add(Activation('relu'))
		self.model.add(MaxPooling2D(pool_size=(2, 2)))
		
		self.model.add(Conv2D(filters=128, kernel_size=(3, 3), strides=(1, 1), kernel_regularizer=l2(0.0001), bias_regularizer=l2(0.0001)))
		self.model.add(Activation('relu'))
		self.model.add(MaxPooling2D(pool_size=(2, 2)))
		

		#self.model.add(Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), kernel_regularizer=l2(0.01), bias_regularizer=l2(0.01)))
		#self.model.add(Activation('relu'))
		#self.model.add(MaxPooling2D(pool_size=(2, 2)))

		self.model.add(Flatten())
		self.model.add(Dense(32, kernel_regularizer=l2(0.0001), bias_regularizer=l2(0.0001)))
		self.model.add(Activation('relu'))
		self.model.add(Dropout(0.2))

		self.model.add(Dense(16, kernel_regularizer=l2(0.0001), bias_regularizer=l2(0.0001)))
		self.model.add(Activation('relu'))
		self.model.add(Dropout(0.2))
		
		#self.model.add(Dense(8, kernel_regularizer=l2(0.1), bias_regularizer=l2(0.1)))
		#self.model.add(Activation('relu'))
		#self.model.add(Dropout(0.1))
		
		self.model.add(Dense(4))
		self.model.add(Activation('sigmoid'))

	#Generator runs an infinite loop through the entire image stack, specified by the start and end variables. Can be set to "training" or "validating" mode.
	#The generator will export a nummber of images specified by batch_size to the NN, and then destroy that batch from memory. This helps with training 
	#massive data sets that would otherwise fill up RAM and SWAP.
	def generator(self, batch_size, start, end, mode):
		index = start
		while True:
			x = []
			y = []
			
			while len(x) < batch_size:
				index += 1
				if index == end:
					index = start
				image_path = "Images/" + mode + "/"+ str(index) + ".png"
				image = load_img(image_path)
				image_vec = img_to_array(image)
				x.append(image_vec)
				y.append(annotations_list[index])
			x = np.array(x)
			y = np.array(y)

			x /= 255
			yield (x, y)

# main function		
def main():
	mlmodel = ML()

	optimizer=Adam(learning_rate=0.0001)
	mlmodel.model.compile(optimizer=optimizer, loss="mse", metrics=["accuracy"])
	training_generator = mlmodel.generator(batch_size=32, start=950001, end=950085, mode="training")
	validating_generator = mlmodel.generator(batch_size=32, start=950085, end=950095, mode="validating")

	#Steps_per_epoch = 26562, validation_steps=3125
	mlmodel.model.fit_generator(training_generator, steps_per_epoch=500, epochs=30, validation_data=validating_generator, validation_steps=75)
	
	#Saves the model
	mlmodel.model.save("Gezi_Image_Recognition")


main()
